class Cachorro:
	def __init__(self, tamanho=0.0, raca="", cor="", peso=0.0):
		self._tamamho = tamanho
		self._raca = raca
		self._cor = cor
		self._peso = peso

	@property
	def tamanho(self):
		return self._tamamho

	@tamanho.setter
	def tamanho(self, tamanho):
		self._tamamho = tamanho

	@property
	def raca(self):
		return self._raca

	@raca.setter
	def raca(self, raca):
		self._raca = raca

	@property
	def cor(self):
		return self._cor

	@cor.setter
	def cor(self, cor):
		self._cor = cor

	@property
	def peso(self):
		return self._peso

	@peso.setter
	def peso(self, tamanho):
		self._peso = peso

	def latir(self):
		print("Au Au Au Au")

	def comer(self):
		print("crunch crunch")
